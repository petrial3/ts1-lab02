package cz.cvut.fel.ts1;

public class Factorial {
    public long factorial(int n) {
        if (n < 0) return 0;
        if (n == 0) return 1;
        long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}
